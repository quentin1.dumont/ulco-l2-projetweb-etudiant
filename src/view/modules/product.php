<?php
    $product = $params["product"][0];
    $comments = $params["comments"];
?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="../../../public/images/<?= $product["image"] ?>">
            <div class="product-miniatures">
                <div><img src="../../../public/images/<?= $product["image"] ?>"></div>
                <div><img src="../../../public/images/<?= $product["image_alt1"] ?>"></div>
                <div><img src="../../../public/images/<?= $product["image_alt2"] ?>"></div>
                <div><img src="../../../public/images/<?= $product["image_alt3"] ?>"></div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?= $product["name"]?></p>
            <h1><?= $product["product_name"]?></h1>
            <p><?= $product["price"]?>€</p>
            <form method="post" action="/cart/add">
                <input type="hidden" name="productid" value="<?=$params["id"]?>">
                <button type="button" id="-">-</button>
                <button type="button" name="quantity" id="number">1</button>
                <input type="hidden" name="quantity" id="quantity" value="1">
                <button type="button" id="+">+</button>
                <input type="submit" value="Ajouter au panier">
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?= $product["spec"]?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if(empty($comments)){?>
            <p>Il n'y a pas d'avis pour ce produit</p>
            <?php } else {
                ?><ul><?php
                foreach($comments as $comment){
                    ?>
                    <li>
                        <p class="product-comment-author"><?= $comment["firstname"]?> <?= $comment["lastname"]?></p>
                        <p><?=$comment["content"]?></p>
                    </li><?php
                }
                ?></ul><?php
            }
            if(isset($_SESSION["userid"])){?>
            <form method="post" action="/postComment">
                <input type="hidden" name="productid" value="<?=$params["id"]?>">
                <input type="text" name="content" placeholder="Rédiger un commentaire">
            </form>
            <?php } ?>
        </div>
    </div>
</div>

<script src="../../../public/scripts/product.js"></script>