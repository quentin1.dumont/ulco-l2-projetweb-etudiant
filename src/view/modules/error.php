<div id="error">
    <h1>Oups...</h1>
    <p>On dirait que la page demandée n'existe pas ou vous n'avez pas la permission d'y accéder!</p>
    <a href="/">Retour à l'accueil</a>
</div>
