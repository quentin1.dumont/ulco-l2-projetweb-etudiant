<?php
if(isset($_SESSION['updated'])){
    ?><div class="box info" style="margin:40px;">Tes informations personelles ont été mises à jour!</div><?php
    $_SESSION['updated'] = null;
}
?>
<div id="info">
    <h2>Information du compte</h2>
    <h3>Information personnelles</h3>
    <form method="post" action="/account/update" class="account-update" onsubmit="validSubmit()">
        <p><span>Prénom</span><input type="text" name="firstname" value="<?= $_SESSION['userfirstname'] ?>"></p>
        <p><span>Nom</span><input type="text" name="lastname" value="<?= $_SESSION['userlastname'] ?>"></p>
        <p><span>Adresse mail</span><input type="text" name="mail" value="<?= $_SESSION['usermail'] ?>"></p>
        <input type="submit" value="Modifier mes informations">
    </form>

    <h3>Commandes</h3>
    <div>Tu n'as pas de commande en cours.</div>
</div>
<script src="../../../public/scripts/account.js"></script>
