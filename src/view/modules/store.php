<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" action="/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
  <input type="radio" value="ASC" name="order" /> Croissant <br />
  <input type="radio" value="DESC" name="order" /> Décroissant <br />

  <div><input type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

    <?php foreach ($params["products"] as $product) { ?>
        <div class="card">
            <p class="card-image"><img src="../../../public/images/<?= $product["image"]?>"></p>
            <p class="card-category"><?=$product["name"]?></p>
            <p class="card-title">
                <a href="/store/<?=$product["id"]?>"><?=$product["product_name"]?></a>
            </p>
            <p class="card-price"><?=$product["price"]?>€</p>
        </div>
    <?php } ?>

</div>

</div>
