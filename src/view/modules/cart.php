<div id="cart">

    <h1 style="margin: 50px">Mon panier</h1>
    <?php if(isset($_SESSION['cart'])){
    $cart = $_SESSION['cart'];
    ?>

    <div>
        <?php
        $total = 0;
        foreach ($cart as $product) {

            $infos = \model\StoreModel::infoProduct($product['id'])[0];
            $total += $infos['price'] * $product['quantity']; ?>

            <div class="product">

                <img src="../../../public/images/<?=$infos['image']?>" alt="<?=$infos['product_name']?>">

                <div class = "product-infos">
                    <p class="product-category"><?=$infos['name']?></p>
                    <h3><?=$infos['product_name']?></h3>
                </div>
                <form>
                    <div class="product-infos">
                        <h5 style="font-size: 20px;">Quantité</h5>
                        <div style="display: flex;">
                            <button type="button">-</button>
                            <input type="hidden" name="name" value="<?=$product['id']?>">
                            <button type="button" name="number"><?=$product['quantity']?></button>
                            <input type="hidden" name="quantity" id="quantity" value="<?=$product['quantity']?>">
                            <button type="button">+</button>
                        </div>
                    </div>
                </form>

                <div class="product-infos">
                    <h5>Prix unitaire</h5>
                    <h2 class="product-price"><?=$infos['price']?>€</h2>
                </div>
            </div>
        <?php } ?>
        <div class="total">
            <h2>Prix total du panier :</h2>
            <h3 class="product-price"><?=$total?>€</h3>
        </div>
        <?php }else { ?>
            <div>Le panier est vide.</div>
        <?php } ?>
    </div>
</div>
