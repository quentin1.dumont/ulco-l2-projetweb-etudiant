<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProducts(null, null, null);

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "products" => $products
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product($id) : void {
      $product = \model\StoreModel::infoProduct($id);
      $comments = \model\CommentModel::listComment($id);
      if(empty($product)) header("Location: /store");
      $params = array(
          "title" => "Store",
          "module" => "product.php",
          "product" => $product,
          "comments" => $comments,
          "id" => $id
      );

      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }

  public function search(){
      $text = null;
      $categories = null;
      $order = null;
      if(!empty($_POST['search'])){
          $text = $_POST['search'];
      }
      if(isset($_POST['category'])){
          $categories = $_POST['category'];
      }
      if(isset($_POST['order'])){
            $order = $_POST['order'];
      }
      // Communications avec la base de données
      $products = \model\StoreModel::listProducts($text, $categories, $order);
      $categories = \model\StoreModel::listCategories();

      // Variables à transmettre à la vue
      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );
      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }

}