<?php


namespace controller;


class AccountController
{
    public function account(){
        if(isset($_SESSION['userfirstname'])) header("Location: /error");
        $params = [
            "title"  => "Account",
            "module" => "account.php"
        ];

        \view\Template::render($params);
    }

    public function login(){
        if(isset($_SESSION['userfirstname'])) {
            header("Location: /error");
            exit();
        }
        $mail = $_POST['usermail'];
        $password = $_POST['userpass'];
        $user = \model\AccountModel::login($mail,$password);
        if(!empty($user)) {
            $_SESSION['userid'] = $user[0];
            $_SESSION['userfirstname'] = $user[1];
            $_SESSION['userlastname'] = $user[2];
            $_SESSION['usermail'] = $mail;
            header("Location: /store?");
            exit();
        }
        header("Location: /account?status=login_fail");
        exit();
    }

    public function signin(){
        if(isset($_SESSION['userfirstname'])) {
            header("Location: /error");
            exit();
        }
        $firstname = $_POST['userfirstname'];
        $lastname = $_POST['userlastname'];
        $mail = $_POST['usermail'];
        $password = $_POST['userpass'];

        if(\model\AccountModel::signin($firstname,$lastname,$mail,$password)){
            header("Location: /account?satus=signin_success");
            exit();
        }
        header("Location: /account?status=signin_fail");
        exit();
    }

    public function logout(){
        if(!isset($_SESSION['userfirstname'])) {
            header("Location: /error");
            exit();
        }
        session_destroy();
        header("Location: /account?status=logout_success");
        exit();
    }

    public function infos(){
        if(!isset($_SESSION['userfirstname'])) header("Location: /error");
        $params = [
            "title"  => "Account Infos",
            "module" => "infos.php"
        ];
        \view\Template::render($params);
    }

    public function updateAccount()
    {
        if(!isset($_SESSION['userfirstname'])) {
            header("Location: /error");
            exit();
        }
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $mail = $_POST['mail'];

        \model\AccountModel::accountUpdate($firstname,$lastname,$mail,$_SESSION['userid']);

        $_SESSION['userfirstname'] = $firstname;
        $_SESSION['userlastname'] = $lastname;
        $_SESSION['usermail'] = $mail;
        $_SESSION['updated'] = true;

        header("Location: /account/infos");
        exit();
    }
}