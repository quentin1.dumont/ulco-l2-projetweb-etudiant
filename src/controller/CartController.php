<?php


namespace controller;


class CartController
{
    public function cart(){
        if(!isset($_SESSION['userfirstname'])) header("Location: /error");
        // Variables à transmettre à la vue
        $params = array(
            "title" => "Cart",
            "module" => "cart.php"
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function addToCart(){
        if(!isset($_SESSION['userfirstname'])) {
            header("Location: /error");
            exit();
        }
        $id = $_POST["productid"];
        $quantity = $_POST['quantity'];
        if(isset($_SESSION['cart'][$id])){
            $_SESSION['cart'][$id]['quantity'] += $quantity;
        } else {
            $_SESSION['cart'][$id]['id'] = $id;
            $_SESSION['cart'][$id]['quantity'] = $quantity;
        }
        $_SESSION['product']= null;
        header("Location: /cart");
        exit();
    }
}