<?php


namespace controller;


class CommentController
{
    public function postComment(){
        $product_id = $_POST["productid"];
        $account_id = $_SESSION['userid'];
        $content = $_POST['content'];
        \model\CommentModel::insertComment($product_id,$account_id,$content);
        header("Location: /store/$product_id");
    }
}