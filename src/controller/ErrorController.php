<?php

namespace controller;

class ErrorController {

  public function error(): void
  {
      // Variables à transmettre à la vue
      $params = [
          "title"  => "404 Not Found!",
          "module" => "error.php"
      ];

      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }

}