<?php


namespace model;


class AccountModel
{
     static public function check($firstname, $lastname, $mail, $password){
         if(strlen($firstname) < 2) return false;
         if(strlen($lastname) < 2) return false;
         if(!filter_var($mail, FILTER_VALIDATE_EMAIL));

         $db = \model\Model::connect();
         $sql = "SELECT mail FROM account WHERE mail = '$mail'";
         $req = $db->prepare($sql);
         $req->execute();

         if(!empty($req->fetchAll()[0])) return false;
         if(strlen($password) < 6) return false;
         return true;
    }

    static public function signin($firstname, $lastname, $mail, $password){
        $check = AccountModel::check($firstname, $lastname, $mail, $password);
        $password = password_hash($password,PASSWORD_BCRYPT);
        if($check === true){
            $db = \model\Model::connect();
            $sql = "INSERT INTO account (firstname, lastname, mail, password) VALUES(?, ?, ?, ?)";
            $req = $db->prepare($sql);
            $req->execute(array($firstname, $lastname, $mail, $password));
        }
        return $check;
    }

    static public function login($mail, $password){
        $db = \model\Model::connect();
        $sql = "SELECT id, firstname, lastname, password FROM account WHERE mail = '$mail'";
        $req = $db->prepare($sql);
        $req->execute();
        $result = $req->fetchAll()[0];
        if(password_verify($password,$result[3])) return $result;
        return null;
    }

    static public function accountUpdate($firstname,$lastname,$mail,$id){
        $db = \model\Model::connect();
        $sql = "UPDATE account SET firstname = ?, lastname = ?, mail = ? WHERE id = $id";
        $req = $db->prepare($sql);
        $req->execute(array($firstname, $lastname, $mail));
    }
}