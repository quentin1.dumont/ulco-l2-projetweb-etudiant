<?php


namespace model;


class CommentModel
{
    public static function insertComment($product_id, $account_id, $content)
    {
        $db = \model\Model::connect();
        $sql = "INSERT INTO comment(id_product,id_account,content) VALUES(
                                                          (SELECT id FROM product WHERE id ='$product_id'),
                                                          (SELECT id FROM account WHERE id ='$account_id'),?)";



        $req = $db->prepare($sql);
        $req->execute(array($content));
    }

    public static function listComment($product_id)
    {
        $db = \model\Model::connect();
        $sql = "SELECT firstname,lastname,content FROM account CROSS JOIN comment ON account.id = id_account WHERE id_product = '$product_id'";
        $req = $db->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
}