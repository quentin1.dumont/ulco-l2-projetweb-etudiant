<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static public function listProducts($text,$categories,$order): array{
      $db = \model\Model::connect();
      $msg = "";
      if(isset($text)){
          $msg .= " WHERE product.name LIKE '%$text%'";
      }
      if(isset($categories)) {
          if(isset($msg)){
              $msg .= " AND ";
          } else {
              $msg .= " WHERE ";
          }
          $count = 0;
          foreach ($categories as $category){
              if($count != 0) $msg .= " or ";
              $msg .= "category.name = '$category'";
              $count++;
          }
      }
      if(isset($order)){
            $msg .= " ORDER BY price $order";
      }
      $sql = "SELECT product.id, product.name as product_name, price, image, category.name FROM product INNER JOIN category ON product.category = category.id".$msg;
      $req = $db->prepare($sql);
      $req->execute();

      return $req->fetchAll();
  }

    static public function infoProduct($id): array{
        $db = \model\Model::connect();
        $sql = "SELECT product.name as product_name, price, image, image_alt1, image_alt2, image_alt3, spec, category.name FROM product INNER JOIN category ON product.category = category.id where product.id = $id";

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }
}