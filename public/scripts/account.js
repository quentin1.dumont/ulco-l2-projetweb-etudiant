function validSubmit() {
    if (document.getElementsByClassName("valid").length === 0
        || document.getElementsByClassName("invalid").length > 0) return false;
    return true;
}

document.addEventListener('DOMContentLoaded', function () {
    let form = document.getElementsByClassName("account-update")[0];
    let firstname = form.firstname;
    let lastname = form.lastname;
    let mail = form.mail;
    var mailExp = new RegExp(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i);

    firstname.addEventListener('change', function () {
        if ((firstname.value).length >= 2) {
            if (firstname.classList.contains("invalid")) {
                firstname.classList.remove("invalid");
                firstname.previousElementSibling.classList.remove("invalid");
            }
            firstname.classList.add("valid");
            firstname.previousElementSibling.classList.add("valid");
        } else {
            if (firstname.classList.contains("valid")) {
                firstname.classList.remove("valid");
                firstname.previousElementSibling.classList.remove("valid");
            }
            firstname.classList.add("invalid");
            firstname.previousElementSibling.classList.add("invalid");
        }
    })

    lastname.addEventListener('change', function () {
        if ((lastname.value).length >= 2) {
            if (lastname.classList.contains("invalid")) {
                lastname.classList.remove("invalid");
                lastname.previousElementSibling.classList.remove("invalid");
            }
            lastname.classList.add("valid");
            lastname.previousElementSibling.classList.add("valid");
            nbError--;
        } else {
            if (firstname.classList.contains("valid")) {
                lastname.classList.remove("valid");
                lastname.previousElementSibling.classList.remove("valid");
                nbError++;
            }
            lastname.classList.add("invalid");
            lastname.previousElementSibling.classList.add("invalid");
        }
    })

    mail.addEventListener('change',function(){
        console.log("BOOM!");
        if(mailExp.test(mail.value)){
            if(mail.classList.contains("invalid")){
                mail.classList.remove("invalid");
                mail.previousElementSibling.classList.remove("invalid");
            }
            mail.classList.add("valid");
            mail.previousElementSibling.classList.add("valid");
            nbError--;
        } else {
            if(mail.classList.contains("valid")){
                mail.classList.remove("valid");
                mail.previousElementSibling.classList.remove("valid");
            }
            mail.classList.add("invalid");
            mail.previousElementSibling.classList.add("invalid");
        }
    })
})