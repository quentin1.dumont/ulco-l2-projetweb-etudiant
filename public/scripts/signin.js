function validSubmit() {
    if (document.getElementsByClassName("valid").length === 5) return true;
    return false;
}

document.addEventListener('DOMContentLoaded', function () {
    let form = document.getElementsByClassName("account-signin")[0];
    let firstname = form.userfirstname;
    let lastname = form.userlastname;
    let mail = form.usermail;
    let password = document.getElementsByName("userpass");
    var mailExp = new RegExp(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i);
    var passwordExp= new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);




    firstname.addEventListener('change', function () {
        if ((firstname.value).length >= 2) {
            if (firstname.classList.contains("invalid")) {
                firstname.classList.remove("invalid");
                firstname.previousElementSibling.classList.remove("invalid");
            }
            firstname.classList.add("valid");
            firstname.previousElementSibling.classList.add("valid");
        } else {
            if (firstname.classList.contains("valid")) {
                firstname.classList.remove("valid");
                firstname.previousElementSibling.classList.remove("valid");
            }
            firstname.classList.add("invalid");
            firstname.previousElementSibling.classList.add("invalid");
        }
    })

    lastname.addEventListener('change', function () {
        if ((lastname.value).length >= 2) {
            if (lastname.classList.contains("invalid")) {
                lastname.classList.remove("invalid");
                lastname.previousElementSibling.classList.remove("invalid");
            }
            lastname.classList.add("valid");
            lastname.previousElementSibling.classList.add("valid");
        } else {
            if (firstname.classList.contains("valid")) {
                lastname.classList.remove("valid");
                lastname.previousElementSibling.classList.remove("valid");
            }
            lastname.classList.add("invalid");
            lastname.previousElementSibling.classList.add("invalid");
        }
    })

    mail.addEventListener('change',function(){
        if(mailExp.test(mail.value)){
            if(mail.classList.contains("invalid")){
                mail.classList.remove("invalid");
                mail.previousElementSibling.classList.remove("invalid");
            }
            mail.classList.add("valid");
            mail.previousElementSibling.classList.add("valid");
        } else {
            if(mail.classList.contains("valid")){
                mail.classList.remove("valid");
                mail.previousElementSibling.classList.remove("valid");
            }
            mail.classList.add("invalid");
            mail.previousElementSibling.classList.add("invalid");
        }
    })

    password[1].addEventListener('change',function() {
        if (passwordExp.test(password[1].value)) {
            if (password[1].classList.contains("invalid")) {
                password[1].classList.remove("invalid");
                password[1].previousElementSibling.classList.remove("invalid");
            }
            password[1].classList.add("valid");
            password[1].previousElementSibling.classList.add("valid");
        } else {
            if (password[1].classList.contains("valid")) {
                password[1].classList.remove("valid");
                password[1].previousElementSibling.classList.remove("valid");
            }
            password[1].classList.add("invalid");
            password[1].previousElementSibling.classList.add("invalid");
        }
    })

    password[2].addEventListener('change',function() {
        if (password[2].value === password[1].value) {
            if (password[2].classList.contains("invalid")) {
                password[2].classList.remove("invalid");
                password[2].previousElementSibling.classList.remove("invalid");
            }
            password[2].classList.add("valid");
            password[2].previousElementSibling.classList.add("valid");
        } else {
            if (password[2].classList.contains("valid")) {
                password[2].classList.remove("valid");
                password[2].previousElementSibling.classList.remove("valid");
            }
            password[2].classList.add("invalid");
            password[2].previousElementSibling.classList.add("invalid");
        }
    })

})