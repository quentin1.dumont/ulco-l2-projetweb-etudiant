document.addEventListener("DOMContentLoaded",function () {
    let numberButton = document.getElementById("number");
    let downButton = document.getElementById("-");
    let upButton = document.getElementById("+");
    let number = 1;
    let quantity = document.getElementById("quantity")
    let boxError = false;

    downButton.addEventListener("click",function () {
        if(number > 1){
            number --;
            numberButton.innerHTML = number;
            quantity.setAttribute('value',number);
        }
        if(boxError === true){
            downButton.parentElement.removeChild(downButton.parentElement.lastChild);
            boxError = false;
        }
    })

    upButton.addEventListener("click",function () {
        if(number < 5){
            number ++;
            numberButton.innerHTML = number;
            quantity.setAttribute('value',number);
        }
        if (number === 5 && boxError === false){
            let newDiv = document.createElement("div");
            newDiv.classList.add("box");
            newDiv.classList.add("error");
            newDiv.innerHTML = "Quantité maximale autorisée !";
            upButton.parentElement.appendChild(newDiv);
            boxError = true;
        }
    })

    let divMiniatures = document.querySelector(".product-miniatures");
    let miniatures = divMiniatures.getElementsByTagName("img");
    let img = divMiniatures.parentElement.firstElementChild;
    for (let miniature of miniatures){
        miniature.addEventListener('click',function(){
            img.setAttribute('src',miniature.src);
        })
    }
})